# Description
The goal of this mattermost/slack webhook integration is to make alerts more customizable and prettier. One of my complaints with the existing slack Nagios integration was the limited number of variables I could pass to slack. Also, I wanted links to the alert and to nagios in each alert. All of this is accomplished in this webhook integration.

### Example alerts
![Alerts](http://i.imgur.com/6QI85mg.png)

# Installation
* I'm assuming that you already have a good understanding of how nagios works, that won't be covered here.
* Create a basic incoming webhook in slack or mattermost or other compatible chat servers.
* Copy the .sh to wherever your nagios plugins live. Typically `/usr/local/nagios/libexec/` (if you've compiled from source).
* Set up the command like the examples below using the URL provided by your incoming webhook config on the chat server.


## Command Definitions
    define command {
            command_name notify_service
            command_line $USER1$/nagios-mattermost-webhook.sh --nagioshost $(echo $HOSTNAME)  --hostalias "$HOSTNAME$" --servicedesc "$SERVICEDESC$" --servicestate "$SERVICESTATE$" --serviceoutput "$SERVICEOUTPUT$" --servicenotes "$SERVICENOTES$" --notificationtype "$NOTIFICATIONTYPE$" --serviceorhost service --webhookusername Nagios --webhookchannel '#alerts' --webhookurl 'https://mattermost.example.org/hooks/xxxxxxxxxxxxxxxxxxx'
    }
    
    define command {
            command_name notify_host
            command_line $USER1$/nagios-mattermost-webhook.sh --nagioshost $(echo $HOSTNAME) --hostalias "$HOSTNAME$" --hoststate "$HOSTSTATE$" --hostoutput "$HOSTOUTPUT$" --hostnotes "$HOSTNOTES$" --notificationtype "$NOTIFICATIONTYPE$" --serviceorhost host --webhookusername Nagios --webhookchannel '#alerts' --webhookurl 'https://mattermost.example/hooks/xxxxxxxxxxxxxxxxxxx'
    }
    
## Usage from terminal output
    If service:
        nagios-mattermost-webhook.sh
        --nagioshost        $(echo $HOSTNAME) (used to build the nagios link URLs. Must be your ngios host fqdn. Ex. nagios.example.com)
        --hostalias         "$HOSTNAME$"
        --servicedesc       "$SERVICEDESC$"
        --servicestate      "$SERVICESTATE$"
        --serviceoutput     "$SERVICEOUTPUT$"
        --servicenotes      "$SERVICENOTES$"
        --notificationtype  "$NOTIFICATIONTYPE$"
        --serviceorhost     service
        --webhookusername   <username>
        --webhookchannel    '<#channel>' (must use single quotes or escape the #)
        --webhookurl        http://<nagios or mattermost webhook url>
    
    If Host:
        nagios-mattermost-webhook.sh
        --nagioshost        $(echo $HOSTNAME) (used to build the nagios link URLs. Must be your ngios host fqdn. Ex. nagios.example.com)
        --hostalias         "$HOSTNAME$"
        --hoststate         "$HOSTSTATE$"
        --hostoutput        "$HOSTOUTPUT$"
        --hostnotes         "$HOSTNOTES$"
        --notificationtype  "$NOTIFICATIONTYPE$"
        --serviceorhost     host
        --webhookusername   <username>
        --webhookchannel    '<#channel>' (must use single quotes or escape the #)
        --webhookurl        http://<nagios or mattermost webhook url>